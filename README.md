# CDG examples

This repository contains examples for the
[depgram](https://gitlab.inria.fr/piemarti/depgram)
and [cdg](https://gitlab.inria.fr/piemarti/cdg) libraries.

You can build the executables with dune by running `dune build` in the
directory. The dependencies are:
- OCaml >= 4.8
- [depgram](https://gitlab.inria.fr/piemarti/depgram)
- [cdg](https://gitlab.inria.fr/piemarti/cdg)
- xml-light

Then you can run an executable with:

    dune exec ./src/executable_name.exe

## Executables

### cdg_of_conllu

Produces a CDG lexicon in txt format from a CoNLL-U corpus of sentences.

Usage:

    dune exec ./src/cdg_of_conllu.exe corpus.conllu

### xml_of_conllu

Produces a UD corpus in XML format with CDG types from a CoNLL-U corpus of
sentences.

Usage:

    dune exec ./src/cdg_of_conllu.exe corpus.conllu

The DTD of the xml document is given by `dune exec ./src/schema.exe`.

### parse

Parses a sentence with a CDG corpus in XML.

Usage:

    dune exec ./src/parse.exe corpus.xml words...
    
Where `corpus.xml` is an xml CDG lexicon (with the DTD given by `schema`) and
`words...` are the words of the sentence.

### parse_example

Uses the parsing algorithm on an simple example (given in the source code).

Usage:

    dune exec ./src/parse_example.exe

### parse_tests

Tests the parsing algorithm on an XML corpus by checking that the parsing
algorithm returns the type given by the corpus.

Usage:

    dune exec ./src/parse_example.exe corpus.xml

### analyse

Analyse a CDG corpus in XML format according to the following metrics:
- total number of words
- total number of distinct types
- proportion of words that can have a type containing a star
- proportion of types with a star whose star is on the left
- proportion of types with a star whose star is on the right
- proportion of types with a star whose star is on both sides
- list of types that can be starred with number of occurrences and left/right
  proportions
- list of types that can govern a starred type with number of occurrences and
  left/right proportions

Usage:

    dune exec ./src/analyse.exe corpus.xml
