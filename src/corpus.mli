val dtd : Dtd.dtd
val entry_point : string
val prove : Xml.xml -> Xml.xml
val dtd_string : string

module Make : sig
  val item : string -> string -> Xml.xml

  val id : Depgram.UD.Id.t -> Xml.xml
  val form : string -> Xml.xml
  val lemma : string -> Xml.xml
  val upos : Depgram.UD.PoS.t -> Xml.xml
  val xpos : string -> Xml.xml
  val feats : string Depgram.UD.Dict.t -> Xml.xml
  val head : Depgram.UD.Id.t -> Xml.xml
  val deprel : Depgram.UD.Deprel.t -> Xml.xml
  val dep : (Depgram.UD.Id.t * Depgram.UD.EnhancedDeprel.t) -> Xml.xml
  val deps : Depgram.UD.Deps.t -> Xml.xml
  val misc : string Depgram.UD.Dict.t -> Xml.xml

  val prim : Cdg.Type.primitive -> Xml.xml
  val kstar_prim : Cdg.KStar.primitive -> Xml.xml
  val valency : Cdg.Valency.t -> Xml.xml

  val left : Cdg.KStar.primitive list -> Xml.xml
  val type_head : Cdg.Type.primitive -> Xml.xml
  val right : Cdg.KStar.primitive list -> Xml.xml
  val potential : Cdg.Valency.t list -> Xml.xml

  val kstar : Cdg.KStar.t -> Xml.xml
end

module Get : sig
  open Xml_combinators

  val id : (Xml.xml, Depgram.UD.Id.t) filter
  val form : (Xml.xml, string) filter
  val lemma : (Xml.xml, string) filter
  val upos : (Xml.xml, Depgram.UD.PoS.t) filter
  val xpos : (Xml.xml, string) filter
  val feats : (Xml.xml, string Depgram.UD.Dict.t) filter
  val head : (Xml.xml, Depgram.UD.Id.t) filter
  val deps : (Xml.xml, Depgram.UD.Deps.t) filter
  val misc : (Xml.xml, string Depgram.UD.Dict.t) filter

  val word_type : (Xml.xml, Cdg.KStar.t) filter
end
