open Cdg
open Depgram.UD
open Xml_combinators
module Dict = Map.Make(String)

let merge t ts =
  if List.exists (fun t' -> KStar.subsumes t' t) ts
  then ts
  else t :: (List.filter (fun t' -> not (KStar.subsumes t t')) ts)

let update t entry =
  match entry with
  | Some ts -> Some (merge t ts)
  | None -> Some [t]

let insert dict (w, t) = Dict.update w (update t) dict

let word_with_type = (this /> Corpus.Get.form
                      &&& this /> Corpus.Get.word_type)

let lexicon (document : Xml.xml) : (KStar.t list) Dict.t =
  document
  |> (this //> is_tag "word" >>> word_with_type)
  |> List.fold_left insert Dict.empty

let all_types (document : Xml.xml) : KStar.t list =
  document
  |> (this //> Corpus.Get.word_type)

let merge_types (types : KStar.t list) : KStar.t list =
  List.fold_left (fun ts t -> merge t ts) [] types

let is_star prim = match prim with
  | KStar.Star _ -> true
  | _ -> false

let has_star_left t = List.exists is_star KStar.(t.left)
let has_star_right t = List.exists is_star KStar.(t.right)
let has_star t = has_star_left t || has_star_right t

let left_stars t = List.filter is_star KStar.(t.left)
let right_stars t = List.filter is_star KStar.(t.right)

let nstars_left t  = List.length (left_stars t)
let nstars_right t = List.length (right_stars t)

let inc_left opt = match opt with
  | Some (t, l, r) -> Some (t+1, l+1, r)
  | None -> Some (1, 1, 0)

let inc_right opt = match opt with
  | Some (t, l, r) -> Some (t+1, l, r+1)
  | None -> Some (1, 0, 1)

let get_star prim = match prim with
  | KStar.Star dep -> Deprel.to_string dep
  | _ -> failwith "(;_;) not a star"

let star_occurrence_incr dict t =
  let dict = left_stars t |> List.fold_left (fun acc prim ->
      Dict.update (get_star prim) inc_left acc
    ) dict
  in
  right_stars t |> List.fold_left (fun acc prim ->
      Dict.update (get_star prim) inc_right acc
    ) dict

let head_occurrence_incr dict t =
  let nl = nstars_left t in
  let nr = nstars_right t in

  Dict.update KStar.(Type.primitive_to_string t.head)
    (fun x -> match x with
      | None -> if nl = 0 && nr = 0 then None else Some (nl, nr)
      | Some (l, r) -> Some (l + nl, r + nr)
    ) dict

let update_cons h t = match t with
  | None -> Some [h]
  | Some t -> Some (h::t)

let () =
  let corpus_file = Array.get Sys.argv 1 in

  let document = Xml.parse_file corpus_file in
  let lexicon = lexicon document in
  let type_occurrences = all_types document in
  let unique_types = merge_types type_occurrences in

  let nwords = Dict.fold (fun _ _ acc -> acc + 1) lexicon 0 in
  let ntypes = List.length unique_types in

  Printf.printf "TOTAL: %d words with %d distinct types\n" nwords ntypes;

  let nstars = (lexicon
             |> Dict.filter (fun _ v -> List.exists has_star v)
             |> Dict.fold (fun _ _ acc -> acc + 1)) 0 in

  Printf.printf "%f of words have a type with a star ( %d / %d )\n"
    Int.(to_float nstars /. to_float nwords)
    nstars
    nwords;

  let with_star = unique_types |> List.filter has_star in
  let with_star_left = with_star |> List.filter has_star_left in
  let with_star_right = with_star |> List.filter has_star_right in
  let with_star_lr = with_star |> List.filter has_star_left |> List.filter has_star_right in
  let nstars = List.length with_star in
  let nstars_l = List.length with_star_left in
  let nstars_r = List.length with_star_right in
  let nstars_lr = List.length with_star_lr in

  Printf.printf "%f of distinct types have a star ( %d / %d )\n"
    Int.(to_float nstars /. to_float ntypes)
    nstars
    ntypes;

  Printf.printf "%f of distinct types with a star have it on the left ( %d / %d )\n"
    Int.(to_float nstars_l /. to_float nstars)
    nstars_l
    nstars;

  Printf.printf "%f of distinct types with a star have it on the right ( %d / %d )\n"
    Int.(to_float nstars_r /. to_float nstars)
    nstars_r
    nstars;

  Printf.printf "%f of distinct types with a star have it on both sides ( %d / %d )\n"
    Int.(to_float nstars_lr /. to_float nstars)
    nstars_lr
    nstars;

  print_endline "\nType | Star occurence | Left star | Right star";

  let star_occurrence = type_occurrences |> List.fold_left star_occurrence_incr Dict.empty in
  star_occurrence |> Dict.iter (fun head (total, left, right) ->
      Printf.printf "%s | %d | %d | %d\n" head total left right
    );

  print_endline "\nType Head | Left stars | Right stars";

  let head_star = type_occurrences |> List.fold_left head_occurrence_incr Dict.empty in
  head_star |> Dict.iter (fun head (l,r) ->
      Printf.printf "%s | %d | %d\n" head l r
    );
