open Cdg
open Xml_combinators
module Dict = Map.Make(String)

let merge t ts =
  if List.exists (fun t' -> KStar.subsumes t' t) ts
  then ts
  else t :: (List.filter (fun t' -> not (KStar.subsumes t t')) ts)

let update t entry =
  match entry with
  | Some ts -> Some (merge t ts)
  | None -> Some [t]

let insert dict (w, t) = Dict.update w (update t) dict

let word_with_type = (this /> Corpus.Get.form
                      &&& this /> Corpus.Get.word_type)

let lexicon (document : Xml.xml) : (KStar.t list) Dict.t =
  document
  |> (this //> is_tag "word" >>> word_with_type)
  |> List.fold_left insert Dict.empty

let words_and_types (sent : Xml.xml) : (string * KStar.t) list =
  sent |>
  (this /> is_tag "word" >>> (this /> Corpus.Get.form &&& this /> Corpus.Get.word_type))

let rec concat_opts opts = match opts with
  | [] -> []
  | (Some h) :: t -> h :: concat_opts t
  | None :: t -> concat_opts t

let parse_test (corpus : (KStar.t list) Dict.t) (sent : (string * KStar.t) list) =
  let types = List.map snd sent in
  let possible_types = sent |> List.map fst |> List.map (fun w -> Dict.find w corpus) in
  let parsed = Parse.parse possible_types |> List.map Parse.item_to_typed_sentence in

  if List.exists (fun s -> List.for_all2 (=) s types) parsed then
    None
  else
    Some (sent, parsed)

let print_error (sent, parsed) =
  print_endline ((sent |> List.map fst |> String.concat " ") ^ ":\n");
  print_endline ("Actual type:\n  " ^ (sent |> List.map snd |> List.map KStar.to_string |> String.concat " "));
  print_endline ("Inferred possible types:\n -"
                 ^ (parsed |> List.map (List.map KStar.to_string) |> List.map (String.concat " ")
                    |> String.concat "\n -")
                )

let () =
  let corpus_file = Array.get Sys.argv 1 in
  let document = Xml.parse_file corpus_file in
  let lexicon = lexicon document in

  let tuples = document |> (this //> is_tag "sentence")
               |> List.map words_and_types in

  let errors = tuples |> List.map (parse_test lexicon) |> concat_opts in

  errors |> List.iter print_error;
  Printf.printf "Errors: %d / %d" (List.length errors) (List.length tuples);
