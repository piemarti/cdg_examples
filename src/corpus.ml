let elem tag etype = Dtd.DTDElement (tag, (Dtd.DTDChild etype))
let pcdata = Dtd.DTDPCData
let empty_elem tag = Dtd.DTDElement (tag, Dtd.DTDEmpty)
let tag tag = Dtd.DTDTag tag
let opt tag = Dtd.DTDOptional (Dtd.DTDTag tag)
let children children = Dtd.DTDChildren children
let one_or_more child = Dtd.DTDOneOrMore child
let choice children = Dtd.DTDChoice children

let dtd = [
  elem "corpus"     (one_or_more (tag "sentence"));
  elem "sentence"   (one_or_more (tag "word"));
  elem "word"       (children [ tag "id"
                              ; tag "form"
                              ; tag "lemma"
                              ; tag "upos"
                              ; opt "xpos"
                              ; opt "feats"
                              ; tag "head"
                              ; tag "deprel"
                              ; opt "extra_deps"
                              ; opt "misc"
                              ; tag "type"
                              ]);

  elem "id"         pcdata;
  elem "form"       pcdata;
  elem "lemma"      pcdata;
  elem "upos"       pcdata;
  elem "xpos"       pcdata;
  elem "feats"      (one_or_more (tag "item"));
  elem "head"       pcdata;
  elem "deprel"     pcdata;
  elem "extra_deps" (one_or_more (tag "dep"));
  elem "misc"       (one_or_more (tag "item"));
  elem "type"       (children [ opt "left"
                              ; tag "type_head"
                              ; opt "right"
                              ; opt "potential" ]);

  elem "left"       (choice [ tag "depname"
                            ; tag "star"
                            ; tag "anchor_l"
                            ; tag "anchor_r" ] |> one_or_more);

  elem "type_head"  (choice [ tag "depname"
                            ; tag "root"
                            ; tag "anchor_l"
                            ; tag "anchor_r" ]);

  elem "right"      (choice [ tag "depname"
                            ; tag "star"
                            ; tag "anchor_l"
                            ; tag "anchor_r" ] |> one_or_more);

  elem "potential"  (choice [ tag "left_neg"
                            ; tag "left_pos"
                            ; tag "right_neg"
                            ; tag "right_pos" ] |> one_or_more);

  empty_elem "root";
  elem "depname"    pcdata;
  elem "star"       pcdata;
  elem "anchor_l"   pcdata;
  elem "anchor_r"   pcdata;

  elem "left_neg"   pcdata;
  elem "left_pos"   pcdata;
  elem "right_neg"  pcdata;
  elem "right_pos"  pcdata;

  elem "item"       (children [tag "k"; tag "v"]);
  elem "k"          pcdata;
  elem "v"          pcdata;

  elem "dep"        (children [tag "head"; tag "deprel"]);
]

let entry_point = "corpus"

let prove =
  try
    Dtd.prove (Dtd.check dtd) entry_point
  with Dtd.Check_error err -> failwith (Dtd.check_error err)

let dtd_string =
  String.concat "\n" (List.map Dtd.to_string dtd)

module Make = struct
  open Depgram.UD
  open Cdg

  let elem name children = Xml.Element (name, [], children)
  let txt text  = Xml.PCData text
  let telem name pcdata = elem name [(txt pcdata)]

  let item k v = elem "item" [telem "k" k; telem "v" v]
  let dict d = d |> Dict.to_seq |> Seq.map (fun (k,v) -> item k v) |> List.of_seq

  let id id = telem "id" (Id.to_string id)
  let form = telem "form"
  let lemma = telem "lemma"
  let upos pos = telem "upos" (PoS.to_string pos)
  let xpos = telem "xpos"
  let feats feats = elem "feats" (dict feats)
  let head head = telem "head" (Id.to_string head)
  let deprel deprel = telem "deprel" (Deprel.to_string deprel)
  let dep (id, dep) = elem "dep" [head id;
                                  telem "deprel" (EnhancedDeprel.to_string dep)]
  let deps deps = elem "extra_deps" (List.map dep deps)
  let misc misc = elem "misc" (dict misc)

  let prim p = match p with
    | Type.Depname dep -> telem "depname" (Deprel.to_string dep)
    | Type.Anchor (Valency.Left, dep) -> telem "anchor_l" (Deprel.to_string dep)
    | Type.Anchor (Valency.Right, dep) -> telem "anchor_r" (Deprel.to_string dep)
    | Type.Root -> elem "root" []

  let kstar_prim p = match p with
    | KStar.Prim p' -> prim p'
    | KStar.Star dep -> telem "star" (Deprel.to_string dep)

  let valency v = match Valency.(v.direction, v.polarity, v.depname) with
    | Valency.(Left,  Negative, dep) -> telem "left_neg" (Deprel.to_string dep)
    | Valency.(Left,  Positive, dep) -> telem "left_neg" (Deprel.to_string dep)
    | Valency.(Right, Negative, dep) -> telem "right_neg" (Deprel.to_string dep)
    | Valency.(Right, Positive, dep) -> telem "right_neg" (Deprel.to_string dep)

  let left ls = elem "left" (List.map kstar_prim ls)
  let type_head h = elem "type_head" [prim h]
  let right rs = elem "right" (List.map kstar_prim rs)
  let potential pot = elem "potential" (List.map valency pot)

  let kstar t = elem "type" KStar.(
      (if t.left = [] then [] else [left t.left])
      @ [type_head t.head]
      @ (if t.right = [] then [] else [right t.right])
      @ (if t.potential = [] then [] else [potential t.potential])
    )
end

module Get = struct
  open Xml_combinators
  open Depgram.UD
  open Cdg

  let of_string parser of_sexp str =
    try
      let sexp = Angstrom.parse_string ~consume:All parser str in
      match sexp with
      | Ok s -> of_sexp s
      | Error e -> failwith e
    with Failure s -> failwith (s ^ " in " ^ str)

  let id = is_tag "id" /> pcdata >>> arr (of_string Id.parse Id.t_of_sexp)

  let form = is_tag "form" /> pcdata
  let lemma = is_tag "lemma" /> pcdata

  let upos = is_tag "upos" /> pcdata >>> arr (of_string PoS.parse PoS.t_of_sexp)

  let xpos = is_tag "xpos" /> pcdata

  let dict node =
    let item = (is_tag "k" /> pcdata) &&& (is_tag "v" /> pcdata) in
    node |> (this /> is_tag "item" /> item)
    |> List.to_seq |> Dict.of_seq

  let feats = is_tag "feats" >>> arr dict

  let head = is_tag "head" /> pcdata >>> arr (of_string Id.parse Id.t_of_sexp)

  let dep =
    let to_dep (head, deprel) =
      (of_string Id.parse Id.t_of_sexp head
      , of_string EnhancedDeprel.parse EnhancedDeprel.t_of_sexp deprel)
    in

    is_tag "dep" /> ((is_tag "head" /> pcdata) &&& (is_tag "deprel" /> pcdata))
    >>> arr to_dep

  let deps = arr (is_tag "deps" /> dep)

  let misc = is_tag "misc" >>> arr dict

  let deprel = pcdata >>> arr (of_string Deprel.parse Deprel.t_of_sexp)

  let root     = is_tag "root" >>> return (Type.Root)
  let depname  = is_tag "depname"  /> deprel >>> arr Type.depname
  let anchor_l = is_tag "anchor_l" /> deprel >>> arr (Type.anchor Valency.Left)
  let anchor_r = is_tag "anchor_r" /> deprel >>> arr (Type.anchor Valency.Right)
  let star     = is_tag "star"     /> deprel >>> arr KStar.star

  let prim = root <|> depname <|> anchor_l <|> anchor_r
  let kstar_prim = star <|> (prim >>> arr KStar.prim)

  let left_neg  = is_tag "left_neg"  /> deprel >>> arr Valency.left_neg
  let left_pos  = is_tag "left_pos"  /> deprel >>> arr Valency.left_pos
  let right_neg = is_tag "right_neg" /> deprel >>> arr Valency.right_neg
  let right_pos = is_tag "right_pos" /> deprel >>> arr Valency.right_pos

  let valency = left_neg <|> left_pos <|> right_neg <|> right_pos

  let left      = arr (this /> is_tag "left"      /> kstar_prim)
  let type_head = this /> is_tag "type_head" /> prim
  let right     = arr (this /> is_tag "right"     /> kstar_prim)
  let potential = arr (this /> is_tag "potential" /> valency)

  let word_type =
    let kstar (((l, h), r), p) =
      KStar.{left = l; head = h; right = r; potential = p}
    in

    is_tag "type"
    >>> left &&& type_head &&& right &&& potential
    >>> arr kstar
end
