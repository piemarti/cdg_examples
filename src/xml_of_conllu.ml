open Depgram
open Cdg

module Dict = CoNLL.Dict

let wordform node = UD.Word.( (Dependency.word node).form )
let type_of node = Type.vicinity node |> KStar.Global.generalise

let get_ok result =
  match result with
  | Ok o -> o
  | Error e -> failwith e

let elem name children = Xml.Element (name, [], children)
let txt text  = Xml.PCData text
let telem name pcdata = elem name [(txt pcdata)]

let rec concat_opts opts = match opts with
  | [] -> []
  | None :: t -> concat_opts t
  | (Some h) :: t -> h :: (concat_opts t)

let node_to_xml (node : (UD.Word.t, UD.Deprel.t) Dependency.node) : Xml.xml =
  let open UD.Word in
  let w = Dependency.word node in
  let t = type_of node in
  elem "word" (concat_opts [
      Some (Corpus.Make.id w.id);
      Some (Corpus.Make.form w.form);
      Some (Corpus.Make.lemma w.lemma);
      Some (Corpus.Make.upos w.upos);
      if w.xpos = "" then None else Some (Corpus.Make.xpos w.xpos);
      if w.feats = Dict.empty then None else Some (Corpus.Make.feats w.feats);
      Some (Corpus.Make.head w.head);
      Some (Corpus.Make.deprel w.deprel);
      if w.deps = [] then None else Some (Corpus.Make.deps w.deps);
      if w.misc = Dict.empty then None else Some (Corpus.Make.misc w.misc);
      Some (Corpus.Make.kstar t)
  ])

let sentence_to_xml (sentence : (UD.Word.t, UD.Deprel.t) Dependency.t) : Xml.xml =
  elem "sentence" (
    Dependency.nodes sentence |> Seq.map node_to_xml
    |> List.of_seq
  )

let () =
  let ic =
    try open_in (Array.get Sys.argv 1)
    with Invalid_argument _ -> stdin
  in

  let oc = try open_out (Array.get Sys.argv 2)
    with Invalid_argument _ -> stdout
  in

  let sentences = UD.parse ic in

  let corpus = elem "corpus" (
    sentences
    |> Seq.map get_ok
    |> Seq.map UD.Sentence.to_dependency
    |> Seq.map sentence_to_xml
    |> List.of_seq
  ) in

  let _ = Corpus.prove corpus in

  output_string oc (Xml.to_string_fmt corpus);

  if oc <> stdout
  then print_endline ("\\(^u^)/ output written!");
