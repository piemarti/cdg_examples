open Depgram
open Cdg

module Dict = Map.Make(String)

let insert_type t ts =
  if List.exists (fun t' -> KStar.subsumes t' t) ts
  then ts
  else
    t :: List.filter (fun t' -> not (KStar.subsumes t t')) ts

let update_entry t entry = match entry with
  | Some ts -> Some (insert_type t ts)
  | None -> Some [t]

let expand lexicon (word, t) = Dict.update word (update_entry t) lexicon

let wordform node = UD.Word.( (Dependency.word node).form )
let type_of node = Type.vicinity node |> KStar.Simple.generalise

let insert_dict lexicon (word, t) =
  Dict.update word (fun ts -> Some (t :: (Option.to_list ts |> List.concat ))) lexicon

let expand_sentence lexicon sent =
  Dependency.nodes sent
  |> Seq.map (fun node -> (wordform node, type_of node) )
  |> Seq.fold_left expand lexicon

let show_entry (word, ts) =
  word ^ " "
  ^ (ts |> List.map KStar.to_string |> String.concat "\n ")
  ^ "\n"

let () =
  let ic =
    try open_in (Array.get Sys.argv 1)
    with Invalid_argument _ -> stdin
  in

  let oc = try open_out (Array.get Sys.argv 2)
    with Invalid_argument _ -> stdout
  in

  let sentences = UD.parse ic in

  let lexicon = sentences
                |> Seq.map Result.get_ok
                |> Seq.map UD.Sentence.to_dependency
                |> Seq.fold_left expand_sentence Dict.empty
  in

  lexicon
  |> Dict.to_seq
  |> Seq.map show_entry
  |> Seq.iter (output_string oc);

  if oc <> stdout
  then print_endline ("\\(^u^)/ output written!");
