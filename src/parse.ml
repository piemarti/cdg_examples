(* open Depgram.UD *)
open Cdg
open Xml_combinators
module Dict = Map.Make(String)

let merge t ts =
  if List.exists (fun t' -> KStar.subsumes t' t) ts
  then ts
  else t :: (List.filter (fun t' -> not (KStar.subsumes t t')) ts)

let update t entry =
  match entry with
  | Some ts -> Some (merge t ts)
  | None -> Some [t]

let insert dict (w, t) = Dict.update w (update t) dict

let word_with_type = (this /> Corpus.Get.form
                      &&& this /> Corpus.Get.word_type)

let lexicon (filename : string) : (KStar.t list) Dict.t =
  Xml.parse_file filename
  |> (this //> is_tag "word" >>> word_with_type)
  |> List.fold_left insert Dict.empty

let () =
  let (corpus_file, words) =
    match Sys.argv |> Array.to_list with
    | _ :: corpus_file :: words -> (corpus_file, words)
    | _ -> failwith ("(;_;) Usage: " ^ (Array.get Sys.argv 0) ^ " corpus.xml words...")
  in
  print_endline ("Corpus: " ^ corpus_file);
  print_endline ("Sentence: " ^ (String.concat ", " words));

  let lexicon = lexicon corpus_file in

  let sent = words |> List.map (fun w ->
      Dict.find_opt w lexicon |> Option.value ~default:[]
    )
  in

  let inferred = Parse.parse sent |> List.map Parse.item_to_typed_sentence in

  inferred |> List.iter (fun s ->
      List.map KStar.to_string s |> String.concat " "
      |> print_endline
    );

  (* words |> List.iter(fun w ->
   *     let types = Dict.find_opt w lexicon
   *                 |> Option.map (List.map KStar.to_string)
   *                 |> Option.map (String.concat "\n")
   *                 |> Option.value ~default:"Not found"
   *     in
   *
   *     print_endline ("\t" ^ w ^ "\n" ^ types ^ "\n");
   *   ) *)
