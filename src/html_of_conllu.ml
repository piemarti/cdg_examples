open Depgram

module Dict = CoNLL.Dict

let elem name children = Xml.Element (name, [], children)
let txt text  = Xml.PCData text
let telem name pcdata = elem name [(txt pcdata)]

let dep_feats_item (k,v) = telem ("dep-" ^ String.lowercase_ascii k) v
let dep_feats_list d = d |> Dict.to_seq |> Seq.map dep_feats_item |> List.of_seq

let dep_id id = telem "dep-id" (UD.Id.to_string id)
let dep_form = telem "dep-form"
let dep_lemma = telem "dep-lemma"
let dep_upos pos = telem "dep-upos" (UD.PoS.to_string pos)
let dep_xpos = telem "dep-xpos"
let dep_feats feats = elem "dep-feats" (dep_feats_list feats)
let dep_head head = telem "dep-head" (UD.Id.to_string head)
let dep_deprel deprel = telem "deprel" (UD.Deprel.to_string deprel)

let get_ok result =
  match result with
  | Ok o -> o
  | Error e -> failwith e

let rec concat_opts opts = match opts with
  | [] -> []
  | None :: t -> concat_opts t
  | (Some h) :: t -> h :: (concat_opts t)

let node_to_xml (node : (UD.Word.t, UD.Deprel.t) Dependency.node) : Xml.xml =
  let open UD.Word in
  let w = Dependency.word node in
  elem "dep-word" (concat_opts [
      Some (dep_id w.id);
      Some (dep_form w.form);
      Some (dep_lemma w.lemma);
      Some (dep_upos w.upos);
      if w.xpos = "" then None else Some (dep_xpos w.xpos);
      if w.feats = Dict.empty then None else Some (dep_feats w.feats);
      Some (dep_head w.head);
      Some (dep_deprel w.deprel);
  ])

let sentence_to_xml (sentence : (UD.Word.t, UD.Deprel.t) Dependency.t) : Xml.xml =
  elem "dep-sentence" (
    Dependency.nodes sentence |> Seq.map node_to_xml
    |> List.of_seq
  )

let () =
  let ic =
    try open_in (Array.get Sys.argv 1)
    with Invalid_argument _ -> stdin
  in

  let oc = try open_out (Array.get Sys.argv 2)
    with Invalid_argument _ -> stdout
  in

  let sentences = UD.parse ic in

  let corpus = elem "corpus" (
    sentences
    |> Seq.map get_ok
    |> Seq.map UD.Sentence.to_dependency
    |> Seq.map sentence_to_xml
    |> List.of_seq
  ) in

  output_string oc (Xml.to_string_fmt corpus);

  if oc <> stdout
  then print_endline ("\\(^u^)/ output written!");
