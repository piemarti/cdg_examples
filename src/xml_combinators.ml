type ('a, 'b) filter = 'a -> ('b list)

let arr f x = [f x]
let return r _ = [r]
let this x = [x]

let (>>>) f g x = f x |> List.concat_map g

let first  f (x, y) = List.map (fun fx -> (fx, y)) (f x)
let second f (y, x) = List.map (fun fx -> (y, fx)) (f x)

let ( *** ) f g (x,y) =
  let (let*) = fun l f -> List.concat_map f l in
  let* fx = f x in
  let* gy = g y in
  [(fx, gy)]

let (&&&) f g x =
  let (let*) = fun l f -> List.concat_map f l in
  let* fx = f x in
  let* gx = g x in
  [(fx, gx)]

let (<+>) f g x = (f x) @ (g x)

let (<|>) f g x =
  let y = f x in
  if y = []
  then g x
  else y

type xml_filter = (Xml.xml, Xml.xml) filter

let is_a pred =
  fun node -> if pred node
    then [node]
    else []

let is_element node = match node with
  | Xml.Element _ -> [node]
  | _ -> []

let is_pcdata node = match node with
  | Xml.PCData _ -> [node]
  | _ -> []

let is_tag name =
  is_element >>> is_a (fun node -> String.equal name (Xml.tag node))

let tag_name = is_element >>> (arr Xml.tag)
let children = is_element >>> Xml.children
let pcdata = is_pcdata >>> (arr Xml.pcdata)

let (/>) f g = f >>> children >>> g

(* You have to put deep and multi in a lambda otherwise it loops forever
   (because OCaml isn't lazy) *)
let rec deep f = f <|> (children >>> (fun x -> deep f x))

let rec multi f = f <+> (children >>> (fun x -> multi f x))

let (//>) f g = f >>> children >>> deep g
