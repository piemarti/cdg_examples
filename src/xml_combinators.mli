(** Combinators for XML manipulation, inspired from Haskell HXT *)

type ('a, 'b) filter = 'a -> ('b list)
val arr : ('a -> 'b) -> ('a, 'b) filter
val return : 'a -> ('b, 'a) filter
val this : ('a, 'a) filter
val first : ('a, 'b) filter -> (('a * 'c), ('b * 'c)) filter
val second : ('a, 'b) filter -> (('c * 'a), ('c * 'b)) filter
val ( *** ) : ('a, 'b) filter -> ('c, 'd) filter -> (('a * 'c), ('b * 'd)) filter
val ( &&& ) : ('a, 'b) filter -> ('a, 'c) filter -> ('a, ('b * 'c)) filter

val (>>>) : ('a, 'b) filter -> ('b, 'c) filter -> ('a, 'c) filter

val (<+>) : ('a, 'b) filter -> ('a, 'b) filter -> ('a, 'b) filter
val (<|>) : ('a, 'b) filter -> ('a, 'b) filter -> ('a, 'b) filter

type xml_filter = (Xml.xml, Xml.xml) filter

val is_a : (Xml.xml -> bool) -> xml_filter

val is_element : xml_filter
val is_pcdata : xml_filter

val is_tag : string -> xml_filter

val tag_name : (Xml.xml, string) filter
val pcdata : (Xml.xml, string) filter

val children : xml_filter
val (/>) : ('a, Xml.xml) filter -> (Xml.xml, 'b) filter -> ('a, 'b) filter

val deep : (Xml.xml, 'a) filter -> (Xml.xml, 'a) filter
val multi : (Xml.xml, 'a) filter -> (Xml.xml, 'a) filter
val (//>) : xml_filter -> (Xml.xml, 'a) filter -> (Xml.xml, 'a) filter
