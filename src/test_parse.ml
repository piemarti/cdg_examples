open Cdg
open Xml_combinators
module Dict = Map.Make(String)

let merge t ts =
  if List.exists (fun t' -> KStar.subsumes t' t) ts
  then ts
  else t :: (List.filter (fun t' -> not (KStar.subsumes t t')) ts)

let update t entry =
  match entry with
  | Some ts -> Some (merge t ts)
  | None -> Some [t]

let insert dict (w, t) = Dict.update w (update t) dict

let word_with_type = (this /> Corpus.Get.form
                      &&& this /> Corpus.Get.word_type)

let lexicon (document : Xml.xml) : (KStar.t list) Dict.t =
  document
  |> (this //> is_tag "word" >>> word_with_type)
  |> List.fold_left insert Dict.empty

let words_and_types (sent : Xml.xml) : (string * KStar.t) list =
  sent |>
  (this /> is_tag "word" >>> (this /> Corpus.Get.form &&& this /> Corpus.Get.word_type))

let parse_test (corpus : (KStar.t list) Dict.t) (sent : (string * KStar.t) list) =
  let types = List.map snd sent in
  let possible_types = sent |> List.map fst |> List.map (fun w -> Dict.find w corpus) in
  let parsed = Parse.parse possible_types |> List.map Parse.item_to_typed_sentence in

  if not (List.exists (fun s -> List.for_all2 (=) s types) parsed) then (
    print_endline "Discrepancy!";
    print_endline ("Sentence: " ^ (List.map fst sent |> String.concat " "));
    print_endline ("Actual type: " ^ (List.map snd sent |> List.map KStar.to_string |> String.concat " "));
    print_endline ("Possible Inferred types:\n- "
                   ^ (parsed |> List.map (List.map KStar.to_string)
                      |> List.map (String.concat " ")
                      |> String.concat "\n- "));
  )

let () =
  let corpus_file = Array.get Sys.argv 1 in
  let document = Xml.parse_file corpus_file in
  let lexicon = lexicon document in

  let tuples = document |> (this //> is_tag "sentence")
               |> List.map words_and_types in

  tuples |> List.iter (parse_test lexicon)

  (* tuples |> List.iter (fun (wts) ->
   *     (wts |> List.map (fun (w,t) ->
   *         w ^ ": " ^  KStar.to_string t
   *       )
   *     |> String.concat "\n") ^ "\n" |> print_endline;
   *   ) *)
