open Cdg
open Depgram
open Parse

let det   = UD.Deprel.{dtype = Det; subtype = None}
let nsubj = UD.Deprel.{dtype = Nsubj; subtype = None}
let amod  = UD.Deprel.{dtype = Amod; subtype = None}
let obj   = UD.Deprel.{dtype = Obj; subtype = None}
let appos = UD.Deprel.{dtype = Appos; subtype = None}

let c1 = [ KStar.(!det) ]

let c2 = [ KStar.(!det >> !nsubj)
         ; KStar.(!det >> !obj)
         ; KStar.(!det >> lanc appos ^^ [Valency.left_neg appos])
         ; KStar.(!det >> ranc appos ^^ [Valency.right_neg appos])
         ]

let c3 = [ KStar.(!nsubj)
         ; KStar.(!obj)
         ]

let c4 = [ KStar.(!amod) ]

let c5 = [ KStar.(!nsubj >> head Type.Root << !obj)
         ; KStar.(!nsubj >> !amod >> head Type.root << !amod << !obj)
         ; KStar.(lanc appos >> !nsubj >> !amod >> head Type.root << !amod << !obj)
         ; KStar.(!nsubj >> !amod >> head Type.root << !amod << !obj << ranc appos)
         ]

let c6 = [ KStar.(!amod) ]

let c7 = [ KStar.(!nsubj)
         ; KStar.(!obj)
         ; KStar.(!obj ^^ [Valency.left_pos appos])
         ; KStar.(!obj ^^ [Valency.right_pos appos])
         ]

let () =
  let sentence = [c1; c2; c3; c4; c5; c6; c7] in
  let result = parse sentence in

  if result = [] then print_endline "no result :(";

  result
  |> List.map item_to_typed_sentence
  |> List.map (fun tl -> Printf.sprintf "[%s]"
      (List.map KStar.to_string tl |> String.concat " ; "))
  |> String.concat "\n"
  |> print_endline
